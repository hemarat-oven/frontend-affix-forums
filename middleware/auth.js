
module.exports = isLoggedIn = (req, res, next) => {
    if(!req.user){
        res.send("------ NO Authentication -------")
    } else {
        next();
    }
};