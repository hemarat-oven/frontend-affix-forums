const express = require("express");
const connectDB = require("./config/db");
const bodyParser = require('body-parser');

const app = express();
const cors = require('cors')

//authentication
const passport = require('passport');
require('./middleware/passport-setup');
const auth =require("./middleware/auth");
const cookieSession = require('cookie-session');

//Connect MongoDB
connectDB();

// Init Middleware	
app.use(express.json({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors({
  origin:"http://localhost:3000",
  credentials:true,
}));
// app.use(morgan('dev'));

// set up session cookies
app.use(
  cookieSession({
    maxAge: 7 * 24 * 60 * 60 * 1000,
    keys: [process.env.COOKIEKEY],
    saveUninitialized: false,
    resave: false
  })
);

// Initializes passport and passport sessions
app.use(passport.initialize());
app.use(passport.session());

//Router\api
app.get("/", (req, res) => res.send("API Running"));
app.use('/api/auth', require("./routers/api/auth"))
// app.use("/api/user", require("./router/api/user"));
// app.use("/api/auth", require("./router/api/auth"));
// app.use("/api/marketing", require("./router/api/marketing"));
app.get("/api/index",auth, (req, res) => res.send("Login Complete"))



// // Serve static assets in production
// if (process.env.NODE_ENV === 'production') {
//     // Set static folder	
//     app.use(express.static('client/build'));
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//     });
// }

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`server Started on port ${PORT}`));